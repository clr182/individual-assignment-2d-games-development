
//EVENT HANDLING
//Mouse Clicks

function getCoords(){
window.addEventListener('click', function (e){
    var canvas = document.getElementById('game-canvas');

    // alerts with mouse position
        let canvasBoundingRectangle = canvas.getBoundingClientRect();

        for(i = 0; i <= 2; i++){
            if(i == 0){
                mouseX1 = e.clientX - canvasBoundingRectangle.left;
                mouseY1 = e.clientY - canvasBoundingRectangle.top;
            }
            else if(i == 1){
                mouseX2 = e.clientX - canvasBoundingRectangle.left;
                mouseY2 = e.clientY - canvasBoundingRectangle.top;
            }
        }
        // //alert("x:" + mouseX + "     Y:" + mouseY);
        
        takeMousePositionAddToArray(mouseX2, mouseY2);
        //takeOldClickAddToArray();

});

}

//function takes the new click position and adds to an array
function takeMousePositionAddToArray(xCoords, yCoords){
    // let newCoords = [];
    // let oldCoords = [];

    game.oldCoords[0] = game.newCoords[0];
    game.oldCoords[1] = game.newCoords[1];
    console.log("old: " + game.oldCoords);

    game.newCoords[0] = xCoords;
    game.newCoords[1] = yCoords;
    console.log("new: " + game.newCoords);
    
    
}

function takeOldClickAddToArray(oldPositions){

    game.oldCoords.push(oldPositions);


    //console.log("old: " + game.oldCoords);
}

//I want to take the coordinates, save them to two arrays new and old
//array one: newCoords
//array two: oldCoords
//before newCoords is updated I want to save both x & y to the oldCoords array
//then display the old coords and the new ones
function processNewOldClicks(){
    getCoords();

}

function clickedOnSprite(sprite){
    if (sprite == 'character'){
        console.log("this is a character");
    }
}


//event listener to pause the game using "P" key
window.addEventListener('keydown', function(e){
    var key = e.keyCode;

    if(key === game.P_key){
        console.log('hello');
        game.togglePaused();
    }
});










