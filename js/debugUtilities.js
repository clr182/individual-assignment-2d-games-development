
/**
 * Provides useful debug related functions.
 * @class DebugUtilities
 * @author NMCG
 * @since 2/19
 * @version 1.0
 * @see https://codeburst.io/javascript-arrow-functions-for-beginners-926947fc0cdc
 * @see https://codepen.io/Universalist/post/predicates-in-javascript 
 */
class DebugUtilities
{
    /** @param {boolean} debugOn Set to true to show run all invoked functions in this class */
    static debugOn = true;

     /**
     * Outputs a console.log() if debugOn is set to true
     * @static
     * @param {string} msg String message to output
     * @memberof DebugUtilities
     */
    static log(msg)
    {
        //exit if debugOn is false i.e. jump over the log function - note the use of this keyword to refer to a variable outside function but inside class scope.
        if(!this.debugOn)
            return;
        else
            console.log(msg);
    }

    /**
     * Outputs a console.log() if the value returns true using the predicate
     * @static
     * @param {string} msg String message to output
     * @param {number} value Value to output if predicate returns true
     * @param {Function} fnPredicateValue Predicate function to test the value for validity
     * @memberof DebugUtilities
     */
    static log_if_value(msg, value, fnPredicateValue)
    {
        //exit if debugOn is false i.e. jump over the log function - note the use of this keyword to refer to a variable outside function but inside class scope.
        if(!this.debugOn)
            return;

        if(fnPredicateValue(value))
            console.log(msg + " - [" + value + "]");
    }

    /**
     * Outputs a console.log() if the value returns true using the value predicate and time returns true using the time predicate.
     * @static
     * @param {string} msg String message to output
     * @param {number} value Value to output if predicate returns true
     * @param {Function} fnPredicateValue Predicate function to test the value for validity
     * @param {number} time Time constraint on the output, msg is shown only if both predicates are satisfied
     * @param {Function} fnPredicateTime Predicate function to test the time for validity
     * @memberof DebugUtilities
     */
    static log_if_value_and_time(msg, value, fnPredicateValue, time, fnPredicateTime)
    {
        //exit if debugOn is false i.e. jump over the log function - note the use of this keyword to refer to a variable outside function but inside class scope.
        if(!this.debugOn)
            return;

        if(fnPredicateValue(value) && fnPredicateTime(time))
            console.log(msg + " - [" + value + "]");
    }
}