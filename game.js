//So the aim of this project is to make a 2D point
//and click adventure game, similar to games like
//Secrets of Monkey Island, Sam & Max and Maniac Mansion,
//or really any of the old SCUMM engine games

//I was inspired to start this project after playing the point
//and click game, Broken Age by games developers Double Fine.
//I don't know how difficult this will be, but i have an idea
//of how to implement the mechanics. Wish me luck...
//--Christian Rafferty--


//this function holds all my game variables and constructors
var Game = function(){
    this.canvas = document.getElementById('game-canvas'),
    this.context = this.canvas.getContext('2d'),
    this.fpsElement = document.getElementById('fps'),
    this.textBox = document.getElementById('textbox'),
    this.inventory = document.getElementById('inventory'),


    
    //Keycodes
    this.LEFT_ARROW = 37,
    this.RIGHT_ARROW = 39,
    this.UP_ARROW = 38,
    this.DOWN_ARROW = 40,
    this.P_key = 80,


    //Clickable boxes
    //this code I am using to create a clickable field so 
    //the characters can walk along it as a path using a 
    //moveTo() function which takes in X and Y coords
    //this.boxDimensions = [x1, y1, x2, y2],
    this.ITEMBOX = 1,
    this.FLOORBOX = 2,
    this.DOORBOX = 3,

    //Mouse Input
    //use item from inventory

    this.mouseX1 = 0,
    this.mouseY1 = 0,
    this.mouseX2 = 0,
    this.mouseY2 = 0,

    this.xPos = 0,
    this.yPos = 0,

    this.newCoords = [],
    this.oldCoords = [],

    //Game States
    this.paused = false,
    this.PAUSE_CHECK_INTERVAL = 200,
    this.pauseStartTime,
    this.windowHasFocus = true,

    this.gameStarted = false;

    //character
    this.currentPosition = [];


    //images
    //this.item= new Images();
    this.spritesheet = new Image(),

    //audio
    this.itemPickUp = new Audio('audio/itemPickUp.wav');
    this.godsound   = new Audio('audio/godsound.wav');
    this.intro      = new Audio('audio/intro.wav');
    this.cavetheme  = new Audio('audio/cavetheme.wav');
    this.pause      = new Audio('audio/pause.wav');
    this.unpause    = new Audio('audio/unpaused.wav');

    // this.cavetheme.loop =true;
    // this.cavetheme.play();
    // this.intro.play();



    //Time & fps
    this.timeSystem = new TimeSystem(),
    this.timeRate = 1,

    this.lastAnimationFrameTime = 0,
    this.lastFpsUpdateTime = 0,
    this.fps = 60,


    //Sprite sheet cells.............................................

    this.BACKGROUND_CELL_WIDTH= 300;
    this.BACKGROUND_CELL_HEIGHT= 150;

    //TREE
    this.TREE_CELL_WIDTH = 75;
    this.TREE_CELL_HEIGHT= 128;

    //VINE
    this.VINES_CELL_WIDTH = 33;
    this.VINES_CELL_HEIGHT = 31;

    //GENERIC CELL WIDTH & HEIGHT
    this.BLOCKS_CELL_WIDTH = 18;
    this.BLOCKS_CELL_HEIGHT = 16;

    //FOLIAGE
    this.FOLIAGE_CELL_WIDTH = 17;
    this.FOLIAGE_CELL_HEIGHT= 13;

    //CHARACTER
    this.CHARACTER_CELL_WIDTH= 20;
    this.CHARACTER_CELL_HEIGHT= 31;


    //setting up bakground-positions
    this.backgroundCells = [
        {left: 2, top: 129, width: this.BACKGROUND_CELL_WIDTH,
                        height: this.BACKGROUND_CELL_HEIGHT}
    ];

    this.vineCells = [
        {left: 0, top: 1, width: this.VINES_CELL_WIDTH,
                        height: this.VINES_CELL_HEIGHT}
    ];

    this.treeCells=[
        {left: 34, top: 0, width: this.TREE_CELL_WIDTH,
                        height: this.TREE_CELL_HEIGHT}
    ];
    this.stoneCells=[
        {left: 175, top: 32, width: this.BLOCKS_CELL_WIDTH, height: this.BLOCKS_CELL_HEIGHT}, 
    ];

    this.startGrassDirtCells=[
        {left: 112, top: 0, width: this.BLOCKS_CELL_WIDTH, height: this.BLOCKS_CELL_HEIGHT},
    ];

    this.middleGrassDirtCells=[
        {left: 128, top: 0, width: this.BLOCKS_CELL_WIDTH, height: this.BLOCKS_CELL_HEIGHT},
    ];

    this.endGrassDirtCells=[
        {left: 190, top: 0, width: this.BLOCKS_CELL_WIDTH, height: this.BLOCKS_CELL_HEIGHT},
    ];

    this.characterCells=[
        {left: 7, top: 321, width: this.CHARACTER_CELL_WIDTH, height: this.CHARACTER_CELL_HEIGHT},
        {left: 43, top: 321, width: this.CHARACTER_CELL_WIDTH, height: this.CHARACTER_CELL_HEIGHT},
        {left: 79, top: 321, width: this.CHARACTER_CELL_WIDTH, height: this.CHARACTER_CELL_HEIGHT},
        {left: 115, top: 321, width: this.CHARACTER_CELL_WIDTH, height: this.CHARACTER_CELL_HEIGHT},
    ];

    this.waterCells=[
        {left: 126, top: 80, width: this.BLOCKS_CELL_WIDTH, height: this.BLOCKS_CELL_HEIGHT}, 
    ];
    
    

    //Sprite data...................................................
    this.backgroundData = [
        {left: 0, top: 0},
        {left: 33, top: 0},
    ];

    this.treeData = [
        {left: 25, top: 0},
    ];

    this.ladderData = [
        {left: 130, top: 100},
    ];

    this.stoneData = [
        {left: 16,  top: 128},
        {left: 52, top: 128},
        {left: 70, top: 128},
        {left: 34, top: 128},
    ];
    this.vineData = [
        {left: 16, top: 128},

    ];

    this.startGrassDirtData = [
        {left: 1,   top: 135},
        {left: 16,  top: 135},
        {left: 34, top: 135},
        {left: 52, top: 135},
        {left: 70, top: 135},
        {left: 77, top: 135},
    ];

    this.waterData = [
        {left: 104, top: 137 + Math.random()},
        {left: 120, top: 137 + Math.random()},
        {left: 136, top: 137 + Math.random()},
        {left: 152, top: 137 + Math.random()},
        {left: 168, top: 137 + Math.random()},
        {left: 184, top: 137 + Math.random()},
        {left: 200, top: 137 + Math.random()},
        {left: 216, top: 137 + Math.random()},
        {left: 232, top: 137 + Math.random()},
        {left: 248, top: 137 + Math.random()},
        {left: 264, top: 137 + Math.random()},
        {left: 280, top: 137 + Math.random()},
        {left: 296, top: 137 + Math.random()},
    ];

    this.endGrassDirtData = [
        {left: 92, top: 135},
    ];

    this.dirtData = [
        {left: 0,  top:  120},
        {left: 33, top: 120},
        {left: 66, top: 120},
        {left: 99, top: 120},
    ];

    this.characterData = [
        {left: 90, top: 105},
    ];

    //Sprites.......................................................
    this.backgrounds =[];
    this.vines       =[];
    this.ladders     =[];
    this.trees       =[];
    this.dirts       =[];
    this.startGrasss =[];
    this.middleGrasss=[];
    this.endGrasss   =[];
    this.waters      =[];
    this.stones      =[];
    this.characters  =[];
    this.sprites     =[];
    this.sounds      =[];
    this.inventory   =[];



    this.fakeBehavior = {

    };


    this.runBehavior={
        lastAdvanceTime: 0,

        execute: function(sprite, now, fps, 
            context, lastAnimationFrameTime){
               if(sprite.runAnimationRate === 0){
                    return;
               }
               if(this.lastAdvanceTime === 0){
                    this.lastAdvanceTime = now;
               }
               else if(now - this.lastAdvanceTime > 
                    1000/sprite.runAnimationRate){
                        sprite.artist.advance();
                        this.lastAdvanceTime = now;
                    }
            }
    };

}

Game.prototype = {

//Setting up sprites.......................................
    createSprites: function() {
        this.createBackground();
        this.createLadderSprites();
        this.createTreeSprites();
        this.createStoneSprites();
        this.createVineSprites();
        this.createStartGrassSprites();
        this.createWaterSprites();
        this.createEndGrassSprites();
        this.createCharacterSprites();

        this.initializeSprites();

        this.addSpritesToSpriteArray();

    },

    addSpritesToSpriteArray: function() {
        for(var i=0; i < this.backgrounds.length; ++i){
            this.sprites.push(this.backgrounds[i]);
        }
        for(var i=0; i < this.ladders.length; ++i){
            this.sprites.push(this.ladders[i]);
        }
        for(var i=0; i < this.trees.length; ++i){
            this.sprites.push(this.trees[i]);
        }
        for(var i=0; i < this.stones.length; ++i){
            this.sprites.push(this.stones[i]);
        }
        for(var i=0; i < this.vines.length; ++i){
            this.sprites.push(this.vines[i]);
        }
        for(var i=0; i < this.dirts.length; ++i){
            this.sprites.push(this.dirts[i]);
        }
        for(var i=0; i < this.startGrasss.length; ++i){
            this.sprites.push(this.startGrasss[i]);
        }
        for(var i=0; i< this.characters.length; ++i){
            this.sprites.push(this.characters[i]);
        }
        for(var i=0; i < this.waters.length; ++i){
            this.sprites.push(this.waters[i]);
        }
        for(var i=0; i< this.endGrasss.length; ++i){
            this.sprites.push(this.endGrasss[i]);
        }
    },

    addSoundsToSoundArray: function(){
        for(var i=0; i <this.cavetheme.length; i++){
            this.sounds.push(this.cavetheme[i]);
        }
    },

    addToInventory: function(){

    },

    positionSprites: function (sprites, spriteData){
        var sprite;

        for(var i = 0; i < sprites.length; ++i){
            sprite = sprites[i];

           sprite.top  = spriteData[i].top;
           sprite.left = spriteData[i].left;
        }
    },

    positionCharacter: function (sprites, spriteData){
        var sprite;

        for(var i = 0; i < sprites.length; ++i){
            sprite = sprites[i];

           sprite.top  = spriteData[i].top;
           sprite.left = spriteData[i].left;
        }
    },

    initializeSprites: function(){
        this.positionSprites(this.ladders, this.ladderData);
        this.positionSprites(this.trees, this.treeData);
        this.positionSprites(this.dirts, this.dirtData);
        this.positionSprites(this.backgrounds, this.backgroundData);
        this.positionSprites(this.startGrasss, this.startGrassDirtData);
        this.positionSprites(this.waters, this.waterData);
        this.positionSprites(this.endGrasss, this.endGrassDirtData);
        this.positionSprites(this.characters, this.characterData);
        this.positionSprites(this.stones, this.stoneData);
        this.positionSprites(this.vines, this.vineData);
    },

    createBackground: function(){
        var background,
        backgroundArtist = new SpriteSheetArtist(this.spritesheet, this.backgroundCells);

        for(var i = 0; i < this.backgroundData.length; ++i){
            background = new Sprite('background', backgroundArtist, []);

            background.width= this.BACKGROUND_CELL_WIDTH;
            background.height=this.BACKGROUND_CELL_HEIGHT;

           this.backgrounds.push(background);
        }
    },

/* MoveTo() function moves the player from his current position to his final position
takes the current position and adds it to an array
takes the final position and adds it to its own array.

a loop to walk from one position to the other 5 pixels at a time.
*/


    createVineSprites: function(){
        var vine,
        vineArtist = new SpriteSheetArtist(this.spritesheet, this.vineCells);

        for(var i = 0; i < this.vineData.length; ++i){
            vine = new Sprite('vine', vineArtist, []);

           vine.width= this.VINES_CELL_WIDTH;
           vine.height=this.VINES_CELL_HEIGHT;

           this.vines.push(vine);
        }
    },

    createWaterSprites: function(){
        var water,

        waterArtist = new SpriteSheetArtist(this.spritesheet, this.waterCells);

        for(var i = 0; i < this.waterData.length; ++i){
            water = new Sprite('water', waterArtist, []);

           water.width= this.BLOCKS_CELL_WIDTH;
           water.height=this.BLOCKS_CELL_HEIGHT;

           this.waters.push(water);
        }
    },

    createStoneSprites: function(){
        var stone,

        stoneArtist = new SpriteSheetArtist(this.spritesheet, this.stoneCells);

        for(var i = 0; i < this.stoneData.length; ++i){
            stone = new Sprite('stones', stoneArtist, []);

           stone.width = this.BLOCKS_CELL_WIDTH;
           stone.height= this.BLOCKS_CELL_HEIGHT;

           this.stones.push(stone);
        }
    },

    createEndGrassSprites: function(){
        var endGrass,
        endGrassArtist = new SpriteSheetArtist(this.spritesheet, this.endGrassDirtCells);

        for(var i = 0; i < this.endGrassDirtData.length; ++i){
            endGrass = new Sprite('endGrass', endGrassArtist, []);

            endGrass.width= this.BLOCKS_CELL_WIDTH;
            endGrass.height=this.BLOCKS_CELL_HEIGHT;

           this.endGrasss.push(endGrass);
        }
    },
    
    createStartGrassSprites: function(){
        var grass,
        grassArtist = new SpriteSheetArtist(this.spritesheet, this.startGrassDirtCells);

        for(var i = 0; i < this.startGrassDirtData.length; ++i){
            grass = new Sprite('grass', grassArtist, []);

           grass.width= this.BLOCKS_CELL_WIDTH;
           grass.height=this.BLOCKS_CELL_HEIGHT;

           this.startGrasss.push(grass);
        }
    },

    createCharacterSprites: function(){
        

        characterArtist = new SpriteSheetArtist(this.spritesheet, this.characterCells);

        for(var i = 0; i < this.characterData.length; ++i){
            this.character = new Sprite('character', characterArtist, [this.runBehavior]);

           this.character.width = this.CHARACTER_CELL_WIDTH;
           this.character.height= this.CHARACTER_CELL_HEIGHT;
           

           this.characters.push(this.character);
        }
    },

    createDirtSprites: function(){
        var dirt,
        dirtArtist = new SpriteSheetArtist(this.spritesheet, this.dirtCells);

        for(var i = 0; i < this.dirtData.length; ++i){
            dirt = new Sprite('dirt', dirtArtist, []);

            dirt.width= this.DIRT_CELL_WIDTH;
            dirt.height=this.DIRT_CELL_HEIGHT;

           this.dirts.push(dirt);
        }
    },

    createLadderSprites: function(){
        var ladder,
        ladderArtist = new SpriteSheetArtist(this.spritesheet, this.ladderCells);

        for(var i = 0; i < this.ladderData.length; ++i){
            ladder = new Sprite('ladder', ladderArtist, []);

           ladder.width= this.LADDER_CELL_WIDTH;
           ladder.height=this.LADDER_CELL_HEIGHT;

           this.ladders.push(ladder);
        }
    },

    createTreeSprites: function(){
        var tree,
        treeArtist = new SpriteSheetArtist(this.spritesheet, this.treeCells);

        for(var i = 0; i < this.treeData.length; ++i){
            tree = new Sprite('tree', treeArtist, []);

           tree.width= this.TREE_CELL_WIDTH;
           tree.height=this.TREE_CELL_HEIGHT;

           this.trees.push(tree);
        }
    },

    updateSprites: function (now) {
        var sprite;
  
        for (var i=0; i < this.sprites.length; ++i) {
           sprite = this.sprites[i];
  
           if (sprite.visible && this.isSpriteInView(sprite)) {
              sprite.update(now, 
               this.fps, 
               this.context,
               this.lastAnimationFrameTime);
           }
        }
    },

    drawSprites: function() {
       var sprite;

       for (var i=0; i < this.sprites.length; ++i) {
          sprite = this.sprites[i];

          if (sprite.visible && this.isSpriteInView(sprite)) {
             this.context.translate(-sprite.hOffset, 0);
             sprite.draw(this.context);
             this.context.translate(sprite.hOffset, 0);
          }
       }
    },

    
    animate: function(now){
        now = game.timeSystem.calculateGameTime();
        if(game.paused){
            setTimeout(function (){
                requestAnimationFrame(game.animate);
            }, game.PAUSE_CHECK_INTERVAL);
        }
        else{
            game.fps = game.calculateFPS(now);
            game.draw(now);
            game.lastAnimationFrameTime = now;
            requestAnimationFrame(game.animate);
        }
    },

    setTimeRate: function(rate){
        this.timeRate = rate;
        this.timeSystem.setTransducer(function(now){
           return now * game.timeRate;
        });
     },

     calculateFPS: function(now){
        var fps = 1/(now - this.lastAnimationFrameTime) * 1000 * game.timeRate;
        if(now - this.lastFpsUpdateTime > 1000){
            this.lastFpsUpdateTime = now;
            this.fpsElement.innerHTML = fps.toFixed(0) + ' fps';
        }
        
        return fps;
       
    },

    isSpriteInView: function(sprite) {
        return sprite.left + sprite.width > sprite.hOffset &&
               sprite.left < sprite.hOffset + this.canvas.width;
     },

    initializeImages: function (){
        this.spritesheet.src = 'images/spritesheet.png';

        this.spritesheet.onload = function (e) {
            game.draw();
         };
    },


    //setting up clickable floors.................................
    drawFloor: function(){

    },

    draw: function(now){
        // this.drawRoomOne();
        // this.drawRoomTwo();
        // this.drawRoomThree();
        
        this.drawSprites();
        this.updateSprites(now);
        this.setOffsets(now);
    },

    // drawRoomOne: function(){
    //     drawFloor();
    //     drawItem();
    //     drawDoor();
    //     drawNPC();
    // },
    // drawRoomTwo: function(){
    //     drawFloor();
    //     drawItem();
    //     drawDoor();
    //     drawNPC();
    // },
    // drawRoomThree: function(){
    //     drawFloor();
    //     drawItem();
    //     drawDoor();
    //     drawNPC();
    // },


    setOffsets: function(now){
        this.setSpriteOffsets(now);
    },

    
    
    setSpriteOffsets: function (now) {
        var sprite;
     
        // In step with platforms
        this.spriteOffset +=
           this.platformVelocity * (now - this.lastAnimationFrameTime) / 1000;
  
        for (var i=0; i < this.sprites.length; ++i) {
           sprite = this.sprites[i];
  
           if ('runner' !== sprite.type) {
              sprite.hOffset = this.spriteOffset; 
           }
        }
     },

     togglePausedStateOfAllBehaviors: function (now) {
        var behavior;
     
        for (var i=0; i < this.sprites.length; ++i) {
           sprite = this.sprites[i];
  
           for (var j=0; j < sprite.behaviors.length; ++j) {
              behavior = sprite.behaviors[j];
  
              if (this.paused) {
                 if (behavior.pause) {
                    behavior.pause(sprite, now);
                 }
              }
              else {
                 if (behavior.unpause) {
                    behavior.unpause(sprite, now);
                 }
              }
           }
        }
     },

    togglePaused: function(){
        var now = this.timeSystem.calculateGameTime();
        this.paused = !this.paused;

        this.togglePausedStateOfAllBehaviors(now);

        if(this.paused){
            this.pause.play();
            this.pauseStartTime = now;
            console.log("paused");
        }
        else{
            this.unpause.play();
            this.lastAnimationFrameTime += 
            (now - this.pauseStartTime);
        }
    },

    startGame: function(){
        this.initializeImages();
        this.createSprites();
        this.timeSystem.start();
        game.setTimeRate(1.0);
        requestAnimationFrame(this.animate);
        
    },

}



function muteAll() {
    var checkBox = document.getElementById('myCheck');
    if(checkBox.checked == true){
        game.cavetheme.muted = true;
    }
}

//Launch Game

var game = new Game();
getCoords();
game.startGame();
